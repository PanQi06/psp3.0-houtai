function getToken() {
    return localStorage.getItem('salesToken')
}
function setToken(token) {
    localStorage.setItem('salesToken', token)
}
function clearToken() {
    return localStorage.removeItem('salesToken')
}

function getInfo() {
    return localStorage.getItem('salesUserInfo')
}
function setInfo(userInfo) {
    localStorage.setItem('salesUserInfo', userInfo)
}
function clearInfo() {
    return localStorage.removeItem('salesUserInfo')
}

function getUserName() {
    return localStorage.getItem('salesUserName')
}
function setUserName(userName) {
    localStorage.setItem('salesUserName', userName)
}
function clearUserName() {
    return localStorage.removeItem('salesUserName')
}

function getPwd() {
    return localStorage.getItem('salesPwd')
}
function setPwd(pwd) {
    localStorage.setItem('salesPwd', pwd)
}
function clearPwd() {
    return localStorage.removeItem('salesPwd')
}

export default {
   setToken,
   getToken,
   clearToken,
   setInfo,
   getInfo,
   clearInfo,
   setUserName,
   getUserName,
   clearUserName,
   setPwd,
   getPwd,
   clearPwd
}