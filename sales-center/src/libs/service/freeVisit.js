import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
   'getFreeInfoList': '/app/visit/v1/getFreeInfoList',  // 获取自由参观列表信息
   'eidtFreeInfo': '/app/visit/v1/eidtFreeInfo', // 编辑自由参观信息
   'getApplyList': '/app/visit/v1/getApplyList', //获取政府调研列表信息
   'getApplyDetail': '/app/visit/v1/getApplyDetail', //获取政府调研详情信息
   'push': '/app/visit/v1/push', //给领导推送拜访信息
   'getList': '/app/account/v1/getList' //获取管理账号列表
 
};

let res = {

};
function getFreeInfoList () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getFreeInfoList, {}, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function eidtFreeInfo (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.eidtFreeInfo, {
            id: params.id,
            videoUrl: params.videoUrl,
            infoLink: params.infoLink,
            roadUrl: params.roadUrl
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getApplyList (params, page, pagesize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getApplyList, {
            startTime: params.startTime,
            endTime: params.endTime,
            visiter: params.visiter,
            isPush: params.isPush,
            isLeader: params.isLeader,
            page: page,
            pagesize: pagesize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getApplyDetail (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getApplyDetail, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function push (visitType, visitId, uid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.push, {
            visitType: visitType,
            visitId: visitId,
            uid: uid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getList () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getList, {}, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}


export default {
    getFreeInfoList, 
    eidtFreeInfo,
    getApplyList,
    getApplyDetail,
    push,
    getList
}