import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getUsers': '/app/user/v1/getUsers', // 客户列表
    'getUserNum': '/app/user/v1/getUserNum', //获取客户总数
    'add': '/app/user/v1/add', //新增、编辑客户
    'editLabel': '/app/user/v1/editLabel', //编辑客户需求
    'getLabel': '/app/user/v1/getLabel',//精进客户需求显示
    'editLevel': '/app/user/v1/editLevel', //编辑客户级别
    'getDetail': '/app/user/v1/getDetail', //获取客户详情
    'getUserLogs': '/app/user/v1/getUserLogs',//获取客户操作日志
    //客户消息相关接口
    'getUserNews': '/app/usernews/v1/getUserNews',//获取客户消息流
    'addClientNews': '/app/usernews/v1/add',//新增客户消息
    //文件详情接口
    'getQINIUToken': '/app/file/v1/getQINIUToken',//获取qiniutoken
    'uploadImage': '/app/file/v1/uploadImage', //上传文件
};

let res = {

};

function getUsers (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUsers, {
            page: params.currentPage,
            pagesize: params.pagesize,
            level: params.level,
            origin: params.origin,
            startCre: params.startCre,
            endCre: params.endCre,
            startAllot: params.startAllot,
            endAllot: params.endAllot,
            name: params.name,
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getUserNum (status) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserNum, {
            status: status
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function addClient (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.add, {
            uid: params.uid,
            oid: params.oid,
            name: params.name,
            phoneNum: params.phoneNum,
            orgName: params.orgName,
            jobTitle: params.jobTitle,
            label: JSON.stringify(params.label)
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}


function editLabel (userId, oid, label) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.editLabel, {
            userId: userId,
            oid: oid,
            label: label
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getLabel (uid, oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getLabel, {
            uid: uid,
            oid: oid
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function editLevel (userId, oid, level) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.editLevel, {
            userId: userId,
            oid: oid,
            level: level
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getDetail (uid, oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getDetail, {
            uid: uid,
            oid: oid
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getUserLogs (uid, oid,  key) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserLogs, {
            uid: uid,
            oid: oid,
            key: key
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getUserNews (uid, oid,  params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserNews, {
            uid: uid,
            oid: oid,
            page: params.currentPage,
            pagesize: params.pagesize,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function addClientNews(uid, oid, label, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addClientNews, {
            uid: uid,
            oid: oid,
            label: label,
            content: content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getQINIUToken() {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getQINIUToken, {}, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function uploadImage() {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.uploadImage, {}, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
export default {
    getUsers: getUsers,
    getUserNum: getUserNum,
    addClient: addClient,
    editLabel: editLabel,
    getLabel: getLabel,
    editLevel: editLevel,
    getDetail: getDetail,
    getUserLogs: getUserLogs,
    getUserNews: getUserNews,
    addClientNews: addClientNews,
    getQINIUToken: getQINIUToken,
    uploadImage: uploadImage
}