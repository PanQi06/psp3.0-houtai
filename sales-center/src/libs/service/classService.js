import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
   'getSignupList': '/app/visit/v1/getSignupList',  // 获取大课堂报名列表信息
   'signupOrder': '/app/visit/v1/signupOrder',   //大课堂添加线下订单
   'getClassFee': '/app/visit/v1/getClassFee', //获取课堂价格说明信息
   'getSignupDetail': '/app/visit/v1/getSignupDetail',  //获取大课堂报名详情信息
   'getClassFeeList': '/app/visit/v1/getClassFeeList',   //获取大课堂设置收费列表信息
   'eidtClassFee': '/app/visit/v1/eidtClassFee',  //编辑收费信息
    'refund': '/app/visit/v1/refund', //退款意见反馈
    'fillExpress': '/app/visit/v1/fillExpress' , //	填写快递信息
   'setSign': '/app/visit/v1/setSign', //设置是否开启报名按钮
   'signupOrderPayState' : '/app/visit/v1/signupOrderPayState'  //大课堂线下报名设置支付推送财务
  
};

let res = {

};
function getSignupList (params, page, pagesize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getSignupList, {
            startTime: params.startTime,
            endTime: params.endTime,
            name: params.name,
            isPush: params.isPush,
            payType: params.payType,
            invoiceState: params.invoiceState,
            expressState: params.expressState,
            state: params.state,
            isLeader: params.isLeader,
            page: page,
            pagesize: pagesize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function signupOrder (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.signupOrder, {
            name: params.name,
            phoneNum: params.phoneNum,
            gender: params.gender,
            position: params.position,
            schooltime: params.schooltime,
            num: JSON.stringify(params.num),
            actualMoney: params.actualMoney,
            payType:params.payType,
            invoiceState: params.invoiceState,
            invoiceType: params.invoiceType,
            invoiceName: params.invoiceName,
            invoiceNo: params.invoiceNo,
            comAddr: params.comAddr,
            comPhoneNum: params.comPhoneNum,
            bank: params.bank,
            bankAccount: params.bankAccount,
            serviceContent: params.serviceContent,
            serviceMoney: params.serviceMoney,
            getUser: params.getUser,
            getUserPhone: params.getUserPhone,
            areaArray: params.areaArray,
            address: params.address,
            pAdcode: params.pAdcode,
            cAdcode: params.cAdcode,
            dAdcode: params.dAdcode
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getClassFee (params, page, pagesize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getClassFee, {}, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getSignupDetail (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getSignupDetail, {
            id: id
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getClassFeeList () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getClassFeeList, {}, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function eidtClassFee (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.eidtClassFee, {
            id: params.id,
            adult: params.adult,
            child: params.child,
            isSign: params.isSign
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function refund (refundId, state, option) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.refund, {
            refundId: refundId,
            state: state,
            option: option,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function fillExpress (sid, eName, eNo) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.fillExpress, {
            sid: sid,
            eName: eName,
            eNo: eNo,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function setSign (id, isSign) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.setSign, {
            id: id,
            isSign: isSign,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function signupOrderPayState (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.signupOrderPayState, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

export default {
    getSignupList,
    signupOrder,
    getClassFee,
    getSignupDetail,
    getClassFeeList,
    eidtClassFee,
    refund,
    fillExpress,
    setSign,
    signupOrderPayState
}