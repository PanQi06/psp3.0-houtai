import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
   'getAllArea': '/app/area/v1/getAllArea',  // 获取区域
  
   
   
};

let res = {

};
function getAllArea (params, page, pagesize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getAllArea, {}, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}






export default {
    getAllArea,

}