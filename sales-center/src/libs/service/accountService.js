import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'login'             : '/app/account/v1/login2Pwd',// 登录
    'updatePassWord'    : '/app/account/v1/update2Pwd',//修改密码
    'updateName'        : '/app/account/v1/update2Name',//修改名称
    'logout'            : '/app/account/v1/logout',//退出登录
    'getAccount'        : '/app/account/v1/getAccount',//获取用户信息
};

let res = {

};
function login (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.login, {
            phoneNum: params.phoneNum,
            pwd: params.password,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function updatePassWord (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updatePassWord, {
            oldPwd: params.oldPwd,
            password: params.password,
            confirmPwd: params.confirmPwd
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function updateName (name) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateName, {
            name: name
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getAccount () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getAccount, {
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function logout () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.logout, {
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}


export default {
    login,
    updateName,
    updatePassWord,
    getAccount,
    logout
}