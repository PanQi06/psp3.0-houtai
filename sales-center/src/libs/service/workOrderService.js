import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    //工单相关接口
    'getOrders': '/app/order/v1/getOrders', //获取工单信息列表
    'getOrderNum': '/app/order/v1/getOrderNum', //获取工单数量
    'addWorkOrder': '/app/order/v1/add', //客户信息管理新增客户工单
    'getServiceProviders': '/app/order/v1/getServiceProviders', //获取服务分类及服务商列表
    'getDetail': '/app/order/v1/getDetail', //获取工单详情
    'getOrderLogs': '/app/order/v1/getOrderLogs', //获取工单操作日志
    'getUserOrder': '/app/order/v1/getUserOrder',//获取用户工单
    'getProvider': '/app/order/v1/getProvider',// 获取需求对应服务商
    'getOrderStatusBean' : '/app/order/v1/getOrderStatusBean', //获取工单状态信息
    'getContractInfo': '/app/order/v1/getContractInfo',//获取工单合同信息
    'getExpress' : '/app/order/v1/getExpress', //获取快递信息
    'getApply': '/app/order/v1/getApply',//获取用户，服务商请求信息
    'closeOrderDetail': '/app/order/v1/closeOrderDetail',//获取关闭工单详情

    'allotOrder': '/app/order/v1/allotOrder', //分配工单给服务商
    'closeOrder': '/app/order/v1/closeOrder', //关闭工单
    'uploadContract': '/app/order/v1/uploadContract', //上传合同
    'updateApply': '/app/order/v1/updateApply', //同意、拒绝请求
    'feedback': '/app/order/v1/feedback', //工单反馈
    'addExpress': '/app/order/v1/addExpress',//录入快递信息
    'confirmPay': '/app/order/v1/confirmPay',//确认支付
    'userPayPush': '/app/order/v1/userPayPush',// 生成支付链接
    'getExpressInfo': '/app/order/v1/getExpressInfo',// 获取发票收件信息
    'ExportOrders': '/app/order/v1/ExportOrders',// 导出工单
};

let res = {

};
function getOrders (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrders, {
            page: params.currentPage,
            pagesize: params.pagesize,
            stage: params.stage,
            isAllot: params.isAllot,
            name: params.name,
            userPayPush: params.userPayPush,
            payType: params.payType,
            userApply: params.userApply,
            payTimeStart: params.payTimeStart,
            payTimeEnd: params.payTimeEnd,
            contractStatus: params.contractStatus
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getUserOrder (userId, oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserOrder, {
            userId: userId,
            oid: oid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getOrderNum(stage) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderNum, {
            stage: stage
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function addWorkOrder (uid, oid, pid, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addWorkOrder, {
            uid: uid,
            oid: oid,
            pid: pid,
            content: content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getServiceProviders() {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getServiceProviders, {}, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getProvider(oid, label) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getProvider, {
            oid: oid,
            label: label
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getDetail(oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getDetail, {
            oid: oid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getOrderLogs(oid, key) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderLogs, {
            oid: oid,
            key: key
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getOrderStatusBean(oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderStatusBean, {
            oid: oid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getExpress(oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getExpress, {
            oid: oid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getContractInfo(oid, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getContractInfo, {
            oid: oid,
            type, type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getApply(oid, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getApply, {
            oid: oid,
            type, type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function closeOrderDetail(oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.closeOrderDetail, {
            oid: oid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function allotOrder(oid, pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.allotOrder, {
            oid: oid,
            pid: pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function closeOrder(oid, status, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.closeOrder, {
            oid: oid,
            status: status,
            content: content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function uploadContract(oid, params, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.uploadContract, {
            oid: oid,
            contractNo: params.contractNo,
            name: params.name,
            signTime: params.signTime,
            startTime: params.startTime,
            endTime: params.endTime,
            partyA: params.partyA,
            partyB: params.partyB,
            contractUrl: params.contractUrl,
            payment: params.payment,
            paymentWay: params.paymentWay,
            payDesc: params.payDesc,
            service: params.service,
            money: params.money,
            type: type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function updateApply(id, enable, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateApply, {
            id: id,
            enable: enable,
            content: content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function feedback(oid, score, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.feedback, {
            oid: oid,
            score: score,
            content: content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function addExpress(oid, uid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addExpress, {
            oid: oid,
            uid: uid,
            eNo: params.eNo,
            eName: params.eName
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function confirmPay(oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.confirmPay, {
            oid: oid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function userPayPush(oid, money) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.userPayPush, {
            oid: oid,
            money: money
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getExpressInfo(oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getExpressInfo, {
            oid: oid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function ExportOrders(params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.ExportOrders, {
            stage: params.stage,
            isAllot: params.isAllot,
            name: params.name,
            payType: params.payType,
            contractStatus: params.contractStatus
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

export default {
    getUserOrder,
    getOrders,
    getOrderNum,
    addWorkOrder,
    getServiceProviders,
    getProvider,
    getDetail,
    getOrderLogs,
    getOrderStatusBean,
    getExpress,
    getContractInfo,
    getApply,
    closeOrderDetail,
    
    allotOrder,
    closeOrder,
    uploadContract,
    updateApply,
    feedback,
    addExpress,
    confirmPay,
    userPayPush,
    getExpressInfo,
    ExportOrders
}