import axios from 'axios';

let util = {

};
util.title = function (title) {
    title = title ? title + '|管理端' : '管理端';
    window.document.title = title;
};

const ajaxUrl = process.env.NODE_ENV === 'production' ? 'http://47.95.227.23:8340' : '/api'
util.baseUrl = ajaxUrl;

util.ajax = axios.create({
    baseURL: ajaxUrl,
    timeout: 30000,
});

export default util;