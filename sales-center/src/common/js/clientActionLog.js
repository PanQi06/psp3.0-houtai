let formatActionType = function(type) {
	if (type == 0) {
        return '分配客户';
    } else if (type == 1) {
        return '新建客户';
    } else if (type == 2) {
        return '修改客户信息';
    } else if (type == 3) {
        return '修改客户状态';
    } else if (type == 4) {
        return '精进需求';
    } else if (type == 5) {
        return '开始沟通';
    } else if (type == 6) {
        return '生成工单';
    }
}

let formatAction = function(data) {
    let type = data.type;
    let seller = JSON.parse(data.sellerJson);
	if (type == 0) {
        let admin = JSON.parse(data.adminJson);
        return '【' + admin.name + '】分配客户给客户经理【'+ seller.name +'】';
    } else if (type == 1) {
        let user = JSON.parse(data.content);
        if (user.position == '') {
            user.position = '-/-'
        }
        return '客户经理【'+ seller.name +'】创建了客户，客户姓名：' + user.name + '，电话：' + user.phoneNum + '，公司名称：' + user.companyName + '，职位：' + user.position;
    } else if (type == 2) {
        let user = JSON.parse(data.content);
        return '客户经理【'+ seller.name +'】修改了客户，客户姓名：' + user.name + '，电话：' + user.phoneNum + '，公司名称：' + user.companyName + '，职位：' + user.position;
    } else if (type == 3) {
        let user = JSON.parse(data.content);
        let level;
        if (user.level == 1) {
            level = '有效客户';
        } else if (user.level == 2) {
            level = '无效客户'
        }
        return '客户经理【'+ seller.name +'】修改了客户状态，客户状态为：' + level;
    } else if (type == 4) {
        let user = JSON.parse(data.content);
        let label;
        label = user.label;
        return '客户经理【'+ seller.name +'】精进了客户需求，客户需求为：' + label;
    } else if (type == 5) {
        let user = JSON.parse(data.content);
        return '客户经理【'+ seller.name +'】与客户【' + user.name + '】沟通，沟通标签为：' + user.label + '，沟通描述为：' + user.content;
    } else if (type == 6) {
        return '客户经理【'+ seller.name +'】创建工单';
    }
}

export {formatActionType, formatAction}