let formatTimestamp = function(timestamp) {
	var date = new Date(timestamp * 1000);//直接用 new Date(时间戳) 格式转化获得当前时间
	return date.toLocaleDateString().replace(/\//g, "-") + " " + date.toTimeString().substr(0, 8);
}

let formatTime = function(time) {
	 if (time <= 60) {
        time = time + '秒';
        return time;
    } else if (time > 60 && time < 60 * 60) {
        time = parseInt(time / 60) + "分钟";
        return time;
    } else {
        var hour = parseInt(time / 3600) + "小时";
        var minute = parseInt(parseInt(time % 3600) / 60) + "分钟";
        time = hour + minute;
        return time;
    }
}

export {formatTimestamp, formatTime}
