import Vue from 'vue'
import Router from 'vue-router'

import login from '@/view/login/login'
import LoginMainPanel from '@/view/login/view/LoginMainPanel'
// import ForgetPwd from '@/components/ForgetPwd'
import main from '@/view/Main'
import workReady from '@/view/workReady/workReady'
import clientManage from '@/view/client/clientManage'
import clientDetail from '@/view/client/clientDetail'
import workOrderManage from '@/view/workOrder/workOrderManage'
import workOrderDetail from	'@/view/workOrder/workOrderDetail'
import userInfo from '@/view/userInfo/userInfo'
import freeVisitManage from '@/view/freeVisit/freeVisitManage'
import governmentManage from '@/view/government/governmentManage'
import bigClassManage from '@/view/bigClass/bigClassManage'
import bigClassSet from '@/view/bigClassSet/bigClassSet'
import govDatail from '@/view/government/view/govDetail'
import classDatail from '@/view/bigClass/view/classDatail'
import bigClassAdd from '@/view/bigClass/view/BigClassAdd'



Vue.use(Router)

export default new Router({
	routes: [
	{
		path: '/',
		name: 'login',
		component: login,
		redirect: '/login',
		children: [
			{
				path: '/login',
				name: 'loginMainPanel',
				component: LoginMainPanel
			}
		]
	},
	{
		path: '/main',
		name: 'main',
		component: main,
		redirect: '/main/workReady',
		children: [
			{
				path: '/main/workReady',
				name: 'workReady',
				component: workReady
			},
			{
				path: '/main/clientManage',
				name: 'clientManage',
				component: clientManage,

			},
			{
				path: '/main/clientDetail',
				name: 'clientDetail',
				component: clientDetail
			},
			{
				path: '/main/workOrderManage',
				name: 'workOrderManage',
				component: workOrderManage,
			},
			{
				path: '/main/workOrderDetail/',
				name: 'workOrderDetail',
				component: workOrderDetail
			},
			{
				path: '/main/userInfo',
				name: 'userInfo',
				component: userInfo
			},
			{
				path:'/main/freeVisitManage',
				name: 'freeVisitManage',
				component: freeVisitManage
			},
			{
				path:'/main/governmentManage',
				name: 'governmentManage',
				component: governmentManage
			},
			{
				path:'/main/bigClassManage',
				name: 'bigClassManage',
				component: bigClassManage
			},
			{
				path:'/main/bigClassAdd',
				name: 'bigClassAdd',
				component: bigClassAdd
			},
			{
				path:'/main/bigClassSet',
				name: 'bigClassSet',
				component: bigClassSet
			},
			{
				path:'/main/governmentManage/:id',
				name: 'govDatail',
				component: govDatail
			},
			{
				path:'/main/bigClassManage/:id',
				name: 'classDatail',
				component: classDatail
			},
		]
    },
    { path: '*', component: LoginMainPanel }
	]
})
