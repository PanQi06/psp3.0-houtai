import axios from 'axios';
import {DEVELOPMENT, PRODUCTION} from '@/libs/service/config'
let util = {

};
util.title = function (title) {
    title = title ? title + '|管理端' : '管理端';
    window.document.title = title;
};

console.log(process.env.NODE_ENV);
let base_url
if (DEVELOPMENT.keys().length != 0) {
    base_url = DEVELOPMENT.BASE_URL
} else {
    base_url = PRODUCTION.BASE_URL
}
const ajaxUrl = process.env.NODE_ENV === 'production' ? base_url : '/api';
util.baseUrl = ajaxUrl;

util.ajax = axios.create({
    baseURL: ajaxUrl,
    timeout: 30000
});

export default util;