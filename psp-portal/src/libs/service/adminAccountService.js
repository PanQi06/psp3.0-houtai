import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'login': '/app/admin/v1/login', // 登录
    'getAdmin': '/app/admin/v1/getAdmin', //获取管理员信息
    'updateName': '/app/admin/v1/updateName',//更新用户名
    'updatePassWord': '/app/admin/v1/updatePassWord', //更新密码
    'getList': '/app/admin/v1/getList',//获取管理员列表
    'eidtAdmin': '/app/admin/v1/eidt',//新增、编辑管理员
    'restAdminPwd': '/app/admin/v1/restAdminPwd',//重置密码
    'delAdmin': '/app/admin/v1/del',//删除管理员

    'getOrderStatistics': '/app/manage/v1/getOrderStatistics',//获取工单统计数
    'getUserStatistics': '/app/manage/v1/getUserStatistics',//获取用户统计数

    'editRole': '/app/role/v1/editRole',//增加、修改用户角色
    'delRole': '/app/role/v1/delRole',//删除角色信息
    'getRoles': '/app/role/v1/getRoles',//获取角色信息
    'enableRole': '/app/role/v1/enableRole',//启用角色
    'disableRole': '/app/role/v1/disableRole',//禁用角色
    'getResources': '/app/role/v1/getResources',//获取资源列表
    'getRoleResources': '/app/role/v1/getRoleResources',//获取角色资源列表
    'addRoleResources': '/app/role/v1/addRoleResources',//设置角色资源
    'delRoleResources': '/app/role/v1/delRoleResources',//删除角色资源

    'editPlat': '/app/platAccount/v1/edit',//新增、编辑账号信息
    'delPlat': '/app/platAccount/v1/del',//删除账号信息
    'resetPwdPlat': '/app/platAccount/v1/resetPwd',//重置账号账号密码
    'getAccounts': '/app/platAccount/v1/getAccounts',//获取账号信息列表
    'enablePlat': '/app/platAccount/v1/enable',//启用账号信息
    'disablePlat': '/app/platAccount/v1/disable',//禁用账号信息
    'getAccounrRoles': '/app/platAccount/v1/getAccounrRoles',//获取账号角色信息
    'addAccountRoles': '/app/platAccount/v1/addAccountRoles',//设置账号角色
    'delAccountRoles': '/app/platAccount/v1/delAccountRoles',//删除账号角色



};

let res = {

};

function login (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.login, {
            phoneNum: params.phoneNum,
            password: params.password,
            imgCode: params.imgCode,
            device: params.device
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getAdmin () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getAdmin, {}, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function updateName (name) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateName, {
            name: name
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function updatePassWord (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updatePassWord, {
            oldPwd: params.oldPwd,
            newPwd: params.newPwd,
            submitPwd: params.submitPwd
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getList (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getList, {
            key: params.key,
            page: params.currentPage,
            pagesize: params.pagesize
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function eidtAdmin (aid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.eidtAdmin, {
            aid: aid,
            username: params.username,
            phoneNum: params.phoneNum,
            password: params.pwd,
            confirmPwd: params.submitPwd,
            type: params.type,
            pid: params.pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function restAdminPwd (aid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.restAdminPwd, {
            aid: aid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getOrderStatistics () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderStatistics, {}, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getUserStatistics () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserStatistics, {}, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function editRole (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.editRole, {
            id: params.id,
            name: params.name,
            type: params.type,
            desc: params.desc,
            sort: params.sort,
            isEnable: params.isEnable
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function delRole (roleId) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delRole, {
            roleId: roleId,
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getRoles (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getRoles, {
            type: params.type,
            key: params.key,
            page: params.currentPage,
            pagesize: params.pagesize
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function enableRole (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.enableRole, {
            id: id,
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function disableRole (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.disableRole, {
            id: id,
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getResources (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getResources, {
            type: params.type,
            key: params.key,
            page: params.currentPage,
            pageSize: params.pagesize
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getRoleResources (roleId) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getRoleResources, {
            roleId: roleId
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function addRoleResources (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addRoleResources, {
            roleId: params.roleId,
            rids: params.rids
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function delRoleResources (roleId,rids) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delRoleResources, {
            roleId: roleId,
            rids: rids
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function editPlat (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.editPlat, {
            id: params.id,
            name: params.name,
            phone: params.phone,
            type: params.type,
            providerId: params.providerId,
            parkId: params.parkId,
            rids: JSON.stringify(params.rids),
            isEnable: params.isEnable
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function delPlat (id, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delPlat, {
            id: id,
            type: type
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function resetPwdPlat (id, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.resetPwdPlat, {
            id: id,
            type: type
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getAccounts (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getAccounts, {
            type: params.type,
            name: params.name,
            phoneNum: params.phoneNum,
            page: params.currentPage,
            pagesize: params.pagesize,
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function enablePlat (id, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.enablePlat, {
            id: id,
            type: type
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function disablePlat (id, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.disablePlat, {
            id: id,
            type: type
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getAccounrRoles (id, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getAccounrRoles, {
            id: id,
            type: type
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function addAccountRoles (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addAccountRoles, {
            acId: params.acId,
            type: params.type,
            rids: params.rids
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function delAccountRoles (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delAccountRoles, {
            acId: params.acId,
            type: params.type,
            rids: params.rids
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

export default {
    login,getAdmin,updateName,updatePassWord,getList,eidtAdmin,restAdminPwd,getOrderStatistics,getUserStatistics,
    editRole,delRole,getRoles,enableRole,disableRole,getResources,getRoleResources,addRoleResources,delRoleResources,
    editPlat, delPlat, resetPwdPlat, getAccounts, enablePlat, disablePlat, getAccounrRoles, addAccountRoles, delAccountRoles
}