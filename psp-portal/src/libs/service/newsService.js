import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getTypeList': '/app/newsinfo/v1/getTypeList', // 新闻分类列表
    'eidtType': '/app/newsinfo/v1/eidtType', //新增新闻分类
    'delType': '/app/newsinfo/v1/delType', //删除新闻分类
    'getNewsInfoList': '/app/newsinfo/v1/getNewsInfoList', //获取新闻内容列表
    'newsInfoSave': '/app/newsinfo/v1/newsInfoSave', //新闻资讯添加
    'newsInfoDetails': '/app/newsinfo/v1/newsInfoDetails',  //新闻资讯查看详情
    'newsInfoDel': '/app/newsinfo/v1/newsInfoDel', //新闻资讯删除
    'getUserMeterial': '/app/user/v1/getUserMeterial', //资质列表
    'getUserMeterialDetails': '/app/user/v1/getUserMeterialDetails', //获取资质详情
    'getBannerList': '/app/banner/v1/getBannerList', //获取banner列表
    'eidtBanner': '/app/banner/v1/eidtBanner',  //	编辑banner
    'getBannerDetail': '/app/banner/v1/getBanner', //获取banner详情
    'delBanner': '/app/banner/v1/delBanner' // 删除banner
    
};

let res = {

};

function getTypeList (params, page, pageSize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getTypeList, {
          
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function eidtType (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.eidtType, {
            id: params.id,
            name: params.name
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function newsInfoDetails (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.newsInfoDetails, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function delType (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delType, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getNewsInfoList (typeId, title, page, pagesize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getNewsInfoList, {
            typeId: typeId,
            title: title,
            page: page,
            pagesize: pagesize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function newsInfoSave (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.newsInfoSave, {
            id: params.id,
            typeId: params.typeId,
            title: params.title,
            content: params.content,
            newsTime: params.newsTime,
            isShow: params.isShow,
            srcType: params.srcType,
            srcUrl: params.srcUrl,
            srcOrgName: params.srcOrgName
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            console.log(error)
            reject(error);
        });
    });
    return p;
}

function newsInfoDel (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.newsInfoDel, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getUserMeterial (params, page, pagesize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserMeterial, {
            userName: params.userName,
            phoneNum: params.phoneNum,
            startTime: params.startTime,
            endTime: params.endTime,
            page: page,
            pagesize: pagesize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getUserMeterialDetails (uid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserMeterialDetails, {
            uid: uid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getBannerList (page, pagesize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getBannerList, {
            page: page,
            pagesize: pagesize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function eidtBanner (params, page, pagesize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.eidtBanner, {
            id: params.id,
            url: params.url
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getBannerDetail (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getBannerDetail, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function delBanner (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delBanner, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

export default {
    getTypeList, 
    eidtType,
    newsInfoDetails,
    delType,
    getNewsInfoList,
    newsInfoSave,
    newsInfoDel,
    getUserMeterial,
    getUserMeterialDetails,
    getBannerList,
    eidtBanner,
    getBannerDetail,
    delBanner
   
}