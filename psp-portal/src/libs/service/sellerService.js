import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getList': '/app/seller/v1/getList', //获取销售列表
    'eidtSeller': '/app/seller/v1/eidt', //编辑销售
    'resetPwd': '/app/seller/v1/restPwd',//重置销售密码
    'delSeller': '/app/seller/v1/del' //删除销售
};

let res = {

};
function getList (pid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getList, {
            pid: pid,
            page: params.currentPage,
            pagesize: params.pagesize,
            key: params.key,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function eidtSeller(sid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.eidtSeller, {
            sid: sid,
            name: params.username,
            phoneNum: params.phoneNum,
            password: params.pwd,
            pid: params.pid,
            type: params.type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function resetPwd(sid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.resetPwd, {
            sid: sid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function delSeller(sid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delSeller, {
            sid: sid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
export default {
    getList, eidtSeller, resetPwd, delSeller
}