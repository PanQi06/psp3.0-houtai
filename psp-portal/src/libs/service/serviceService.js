import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getAllServices': '/app/service/v1/getAllServices', //获取所有层级服务分类
    'getCategories': '/app/service/v1/getCategories', //获取服务分类
    'getService': '/app/service/v1/getService',//获取服务列表
    'addService': '/app/service/v1/addService',//创建服务
    'editService': '/app/service/v1/editService',//编辑服务

    'delService': '/app/service/v1/delService',//删除服务
};

let res = {

};
function getAllServices () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getAllServices, {}, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getCategories () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getCategories, {}, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getService (parentId) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getService, {
            parentId: parentId
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function addService (isService, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addService, {
            parentId: params.cid,
            isService: isService,
            name: params.name,
            sort: params.sort,
            price: params.price
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function editService (parentId, isService, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.editService, {
            parentId: parentId,
            cid: params.cid,
            isService: isService,
            name: params.name,
            sort: params.sort,
            price: params.price
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function delService (cid, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delService, {
            cid: cid,
            type: type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
export default {
    getAllServices,getCategories,getService,addService,editService,delService
}