import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getSignupList': '/app/visit/v1/getSignupList',//获取大课堂报名列表订单
    'getSignupDetail': '/app/visit/v1/getSignupDetail',//获取大课堂详情
    'getExpressDetail': '/app/visit/v1/getExpressDetail',//获取发票快递信息
    'refund': '/app/visit/v1/refund',//退款意见反馈
    'setInvoice': '/app/visit/v1/setInvoice',//设置发票已开
    'setPay': '/app/visit/v1/setPay',//设置发票已开
    'fillExpress': '/app/visit/v1/fillExpress',//填写快递信息

};

let res = {

};
function getSignupList (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getSignupList, {
            page: params.currentPage,
            pagesize: params.pagesize,
            name: params.name,
            invoiceState: params.invoiceState,
            state: params.state,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getSignupDetail (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getSignupDetail, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getExpressDetail(id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getExpressDetail, {
            id: id
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function refund(params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.refund, {
            refundId: params.refundId,
            state: params.state,
            option: params.option
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function setInvoice(sid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.setInvoice, {
            sid: sid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function setPay(sid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.setPay, {
            sid: sid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function fillExpress(params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.fillExpress, {
            sid: params.sid,
            eName: params.eName,
            eNo: params.eNo
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}


export default {
    getSignupList,
    getSignupDetail,
    getExpressDetail,
    refund,
    setInvoice,
    setPay,
    fillExpress,
}