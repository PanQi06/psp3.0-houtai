import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getCaseNum': '/app/case/v1/getCaseNum', //获取服务案例数量列表
    'getUnlinkedCases': '/app/case/v1/getUnlinkedCases',  //获取当前服务未关联的案例列表
    'addCase2Service': '/app/case/v1/addCase2Service',  //	服务添加服务案例
    'getCaseList': '/app/case/v1/getCaseList', // 获取三级下全部案例列表
    'editCase2Service': '/app/case/v1/editCase2Service', //	服务编辑 服务案例
    'delCase' : '/app/case/v1/delCase',  //删除服务案例
    'editCase': '/app/case/v1/editCase', //	编辑服务案例
    'getAllCaseList': '/app/case/v1/getAllCaseList', //获取同一七星下案例列表
    'getDetail' : '/app/case/v1/getDetail'  //服务案例预览

   
   



};

let res = {

};

function getCaseNum (params, page, pageSize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getCaseNum, {
            schType: params.schType,
            name: params.name,
            page: page,
            pageSize: pageSize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getUnlinkedCases (cid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUnlinkedCases, {
            cid: cid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function addCase2Service (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addCase2Service, {
            cid: params.cid,
            caseIds: JSON.stringify(params.caseIds)
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getCaseList (cid, name, pageData) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getCaseList, {
            cid: cid,
            name: name,
            page: pageData.page,
            pageSize: pageData.pageSize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function editCase2Service (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.editCase2Service, {
            cid: params.cid,
            caseIds: JSON.stringify(params.caseIds)
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function delCase (caseId) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delCase, {
            caseId: caseId,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function editCase (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.editCase, {
            id: params.id,
            cid: params.cid,
            title: params.title,
            content: params.content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getAllCaseList (cid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getAllCaseList, {
            cid: cid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getDetail (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getDetail, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
export default {
    getCaseNum,
    getUnlinkedCases,
    addCase2Service,
    getCaseList,
    editCase2Service,
    delCase,
    editCase,
    getAllCaseList,
    getDetail

}