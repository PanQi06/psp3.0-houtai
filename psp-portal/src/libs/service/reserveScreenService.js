import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getReserveUsers': '/app/reserveUsers/v1/getReserveUsers', // 获取屏幕预定用户列表
    'getScreenParameter': '/app/reserveUsers/v1/getScreenParameter', //获取屏幕参数列表
    'getScreenParameterList': '/app/reserveUsers/v1/getScreenParameterList', //预定用户获取屏幕参数列表
    'delResUsers': '/app/reserveUsers/v1/delResUsers', //删除预定用户
    'delScreenInfo': '/app/reserveUsers/v1/delScreenInfo', //删除屏幕信息
    'selectScreen': '/app/reserveUsers/v1/selectScreen', //选择屏幕
    'modifyScreenInfo': '/app/reserveUsers/v1/modifyScreenInfo', //修改屏幕参数
    'addScreenInfo': '/app/reserveUsers/v1/addScreenInfo', //新增屏幕参数
    'screenConfig': '/app/reserveUsers/v1/screenConfig', //是否展示屏幕
    
};

let res = {

};
function getReserveUsers (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getReserveUsers, {
            page: params.currentPage,
            pagesize: params.pagesize,
            startTime: params.startTime,
            endTime: params.endTime,
            rname: params.rname,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getScreenParameter(params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getScreenParameter, {
            page: params.currentPage,
            pagesize: params.pagesize,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getScreenParameterList(params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getScreenParameterList, {
            startTime: params.startTime,
            endTime: params.endTime,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function delResUsers(rid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delResUsers, {
            rid: rid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function delScreenInfo(sid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delScreenInfo, {
            sid: sid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function selectScreen(params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.selectScreen, {
            rid: params.rid,
            sid: params.sid,
            type: params.type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function modifyScreenInfo(params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.modifyScreenInfo, {
            sid: params.sid,
            name: params.name,
            imgUrl: params.imgUrl,
            screenInfo: JSON.stringify(params.screenInfo),
            isShow: params.isShow,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function addScreenInfo(params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addScreenInfo, {
            name: params.name,
            imgUrl: params.imgUrl,
            screenInfo: JSON.stringify(params.screenInfo),
            isShow: params.isShow,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
export default {
    getReserveUsers, 
    getScreenParameter, 
    getScreenParameterList,
    delResUsers, 
    delScreenInfo,
    selectScreen,
    modifyScreenInfo,
    addScreenInfo,
}