import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    // 'addProvide': '/provider/v1/add', //创建服务商并生成管理员账户
    // 'eidtProvider': '/provider/v1/eidtProvider',//编辑服务商
    
    // 'getList': '/provider/v1/getList', //获取服务商列表
    // 'delProvide': '/provider/v1/del',//删除服务商
    // 'getDetail': '/provider/v1/getDetail',//获取服务商信息
    // 'getAccountList': '/provider/v1/getAccountList',//获取服务商账号列表
    // 'addAccount': '/provider/v1/addAccount',//创建服务商子账号
    // 'restPwd': '/provider/v1/restPwd',//重置密码
    // 'delAccount': '/provider/v1/delAccount', //删除子账户

    'addNewProvider': '/app/provider/v1/addNewProvider', //新增服务商
    'eidtProvider': '/app/provider/v1/eidtProvider', //编辑服务商
    'getProvlderList': '/app/provider/v1/getProvlderList', //获取服务商列表
    'getNewDetail': '/app/provider/v1/getNewDetail', //获取服务商详情
    'getProviderContenList': '/app/provider/v1/getProviderContenList', //获取服务商内容列表
    'getProviderContenDetail': '/app/provider/v1/getProviderContenDetail', //内容列表详情
    'addProviderConten': '/app/provider/v1/addProviderConten', //新增服务内容
    'updateProviderConten': '/app/provider/v1/updateProviderConten', //修改服务内容
    'delProviderConten': '/app/provider/v1/delProviderConten', //删除服务内容
    'addService': '/app/provider/v1/addService',//添加服务分类
    'updateService': '/app/provider/v1/updateService',//修改服务分类
    'delService': '/app/provider/v1/delService',//激活禁用服务分类
    'getNewServiceList': '/app/provider/v1/getNewServiceList',//获取服务商服务列表
    'addProviderContens': '/app/provider/v1/addProviderContens',//新增首页团队介绍
    'updateProviderContens': '/app/provider/v1/updateProviderContens',//修改首页团队介绍
};

let res = {

};
// function addProvide (params) {
//     let p = new Promise(function (resolve, reject) {
//         util.ajax.post(api.addProvide, {
//             name: params.name,
//             address: params.address,
//             contact: params.contact,
//             phoneNum: params.phoneNum,
//             password: params.pwd,
//             confirmPwd: params.submitPwd,
//             content: params.content
//         }, postCfg).then(function(res) {
//             resolve(res);
//         }, function(error) {
//             reject(error);
//         });
//     });
//     return p;
// }
// function eidtProvider (pid, params) {
//     let p = new Promise(function (resolve, reject) {
//         util.ajax.post(api.eidtProvider, {
//             pid: pid,
//             name: params.name,
//             address: params.address,
//             contact: params.contact,
//             phoneNum: params.phoneNum,
//             content: params.content,
//             cid: params.cid,
//         }, postCfg).then(function(res) {
//             resolve(res);
//         }, function(error) {
//             reject(error);
//         });
//     });
//     return p;
// }
function addNewProvider (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addNewProvider, {
            name: params.name,
            address: params.address,
            logo: params.logo,
            contact: params.contact,
            phoneNum: params.phoneNum,
            content: params.content,
            managerLogo: params.managerLogo,
            managerName: params.managerName,
            managerMotto: params.managerMotto,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function eidtProvider (pid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.eidtProvider, {
            pid: pid,
            name: params.name,
            address: params.address,
            logo: params.logo,
            contact: params.contact,
            phoneNum: params.phoneNum,
            content: params.content,
            managerLogo: params.managerLogo,
            managerName: params.managerName,
            managerMotto: params.managerMotto,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getProvlderList (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getProvlderList, {
            page: params.page,
            pagesize: params.pagesize,
            key: params.key,
            startTime: params.startTime,
            endTime: params.endTime
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getNewDetail (pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getNewDetail, {
            pid: pid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getProviderContenList (pid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getProviderContenList, {
            pid: pid,
            page: params.page,
            pagesize: params.pagesize,
            type: params.type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getProviderContenDetail (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getProviderContenDetail, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function addProviderConten (pid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addProviderConten, {
            pid: pid,
            tatleName: params.tatleName,
            content: params.content,
            teamContent: params.teamContent,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function updateProviderConten (id, pid,  params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateProviderConten, {
            id: id,
            pid: pid,
            tatleName: params.tatleName,
            content: params.content,
            teamContent: params.teamContent,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function delProviderConten (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delProviderConten, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function addService (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addService, {
            pid: params.pid,
            cid: params.cid,
            contentId: params.contentId
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function updateService (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateService, {
            id: params.id,
            pid: params.pid,
            cid: params.cid,
            contentId: params.contentId
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function delService (pid, cid, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delService, {
            pid: pid,
            cid: cid,
            type: type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getNewServiceList (pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getNewServiceList, {
            pid: pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function addProviderContens (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addProviderContens, {
            pid: params.pid,
            introduce: params.introduce,
            managerLogo: params.managerLogo,
            managerName: params.managerName,
            managerMotto: params.managerMotto,
            status: params.status,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function updateProviderContens (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateProviderContens, {
            id: params.id,
            pid: params.pid,
            introduce: params.introduce,
            managerLogo: params.managerLogo,
            managerName: params.managerName,
            managerMotto: params.managerMotto,
            status: params.status,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
// function getList (page, pagesize, cid) {
//     let p = new Promise(function (resolve, reject) {
//         util.ajax.post(api.getList, {
//             page: page,
//             pagesize: pagesize,
//             cid: cid
//         }, postCfg).then(function(res) {
//             resolve(res);
//         }, function(error) {
//             reject(error);
//         });
//     });
//     return p;
// }

// function delProvide (pid) {
//     let p = new Promise(function (resolve, reject) {
//         util.ajax.post(api.delProvide, {
//             pid: pid
//         }, postCfg).then(function(res) {
//             resolve(res);
//         }, function(error) {
//             reject(error);
//         });
//     });
//     return p;
// }

// function getDetail (pid) {
//     let p = new Promise(function (resolve, reject) {
//         util.ajax.post(api.getDetail, {
//             pid: pid
//         }, postCfg).then(function(res) {
//             resolve(res);
//         }, function(error) {
//             reject(error);
//         });
//     });
//     return p;
// }

// function getAccountList (page, pagesize, pid) {
//     let p = new Promise(function (resolve, reject) {
//         util.ajax.post(api.getAccountList, {
//             page: page,
//             pagesize: pagesize,
//             pid: pid
//         }, postCfg).then(function(res) {
//             resolve(res);
//         }, function(error) {
//             reject(error);
//         });
//     });
//     return p;
// }
// function addAccount (params, pid) {
//     let p = new Promise(function (resolve, reject) {
//         util.ajax.post(api.addAccount, {
//             name: params.name,
//             phone: params.phone,
//             password: params.password,
//             pid: pid
//         }, postCfg).then(function(res) {
//             resolve(res);
//         }, function(error) {
//             reject(error);
//         });
//     });
//     return p;
// }

// function restPwd (aid) {
//     let p = new Promise(function (resolve, reject) {
//         util.ajax.post(api.restPwd, {
//             aid: aid
//         }, postCfg).then(function(res) {
//             resolve(res);
//         }, function(error) {
//             reject(error);
//         });
//     });
//     return p;
// }

// function delAccount (aid) {
//     let p = new Promise(function (resolve, reject) {
//         util.ajax.post(api.delAccount, {
//             aid: aid
//         }, postCfg).then(function(res) {
//             resolve(res);
//         }, function(error) {
//             reject(error);
//         });
//     });
//     return p;
// }
export default {
    // addProvide,eidtProvider,addService, delService,getServiceList,getList,delProvide,getDetail,getAccountList,addAccount,restPwd,delAccount
    addNewProvider,
    eidtProvider,
    getProvlderList,
    getNewDetail,
    getProviderContenList,
    getProviderContenDetail,
    addProviderConten,
    updateProviderConten,
    delProviderConten,
    addService,
    updateService,
    delService,
    getNewServiceList,
    addProviderContens,
    updateProviderContens,
}