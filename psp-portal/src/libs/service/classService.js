import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getClassSignupList': '/app/visit/v1/getClassSignupList', // 大课堂订单分配列表
    'getSignupDetail': '/app/visit/v1/getSignupDetail', // 大课堂订单分配详情
    'distributionSignup': '/app/visit/v1/distributionSignup',  //大课堂分配
    'getApplyList': '/app/visitStreetApply/v1/getApplyList', // 获取政府调研分配列表
    'getApplyDetail': '/app/visitStreetApply/v1/getApplyDetail', //获取政府调研分配详情信息
    'addApplylecturer': '/app/visitStreetApply/v1/addApplylecturer',//政府调研订单讲师分配
    'getClassTeacher': '/app/visitStreetApply/v1/getClassTeacher'  //获取讲师列表

   
 

};

let res = {

};

function getClassSignupList (params, page, pagesize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getClassSignupList, {
            state: params.state,
            invoice: params.invoice,
            invoiceState: params.invoiceState,
            startTime: params.startTime,
            endTime: params.endTime,
            name: params.name,
            sid: params.sid,
            page: page,
            pagesize: pagesize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getSignupDetail (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getSignupDetail, {
           id: id
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function distributionSignup (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.distributionSignup, {
           id: params.id,
           sid: params.sid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getApplyList (params, page, pagesize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getApplyList, {
           startTime: params.startTime,
           endTime: params.endTime,
           visiter: params.visiter,
           isPush: params.isPush,
           sid: params.sid,
           page: page,
           pagesize: pagesize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getApplyDetail (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getApplyDetail, {
           id: id
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function addApplylecturer (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addApplylecturer, {
           id: params.id,
           sid: params.sid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getClassTeacher () {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getClassTeacher, {}, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}






export default {
    getClassSignupList,
    getSignupDetail,
    distributionSignup,
    getApplyList,
    getApplyDetail,
    addApplylecturer,
    getClassTeacher
   
}