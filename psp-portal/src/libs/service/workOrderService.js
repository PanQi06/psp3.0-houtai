import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    //工单相关接口
    'getOrders': '/app/order/v1/getOrders', //获取工单信息列表
    'getPayOrders': '/app/order/v1/getPayOrders', //获取财务工单信息列表
    'getOrderNum': '/app/order/v1/getOrderNum', //获取工单数量
    'getDetail': '/app/order/v1/getDetail', //获取工单详情
    'getOrderLogs': '/app/order/v1/getOrderLogs', //获取工单操作日志
    'getOrderApply': '/app/order/v1/getOrderApply', //查看用户，服务商申请，关闭工单详情
    'getOrderState': '/app/order/v1/getOrderState', //根据工单id查询发票详情，支付状态详情，快递状态详情
    'getOrderContract': '/app/order/v1/getOrderContract', //根据工单id查询合同详情
    'getOrderStateList': '/app/order/v1/getOrderStateList',//获取工单状态列表
    'updateOrder': '/app/order/v1/updateOrder',//工单重新分配
    'addOrderAllot': '/app/order/v1/addOrderAllot',//分配园区
    'getPayOrder': '/app/order/v1/getPayOrder',//确认收款
    'getOrderInvoice': '/app/order/v1/getOrderInvoice',//开具发票
    'confirmRefund': '/app/order/v1/confirmRefund',// 确认、拒绝退款
    'getOrdeExport': '/app/order/v1/getOrdeExport',// 工单导出
};

let res = {

};
function getOrders (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrders, {
            page: params.currentPage,
            pagesize: params.pagesize,
            providerId: params.providerId,
            key: params.key,
            status: params.status,
            parkId: params.parkId,
            type: params.type,
            dateType: params.dateType,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getPayOrders (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getPayOrders, {
            page: params.currentPage,
            pagesize: params.pagesize,
            type: params.type,
            key: params.key,
            orderPay: params.orderPay,
            isNeedlnvoice: params.isNeedlnvoice,
            refundStatus: params.refundStatus,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getOrderNum(stage) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderNum, {
            stage: stage
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getDetail(oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getDetail, {
            oid: oid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getOrderLogs(oid, key) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderLogs, {
            oid: oid,
            key: key
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getOrderApply(params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderApply, {
            oid: params.oid,
            type: params.type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getOrderState(oid, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderState, {
            oid: oid,
            type: type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getOrderContract(oid, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderContract, {
            oid: oid,
            type: type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function updateOrder(oid, sid, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateOrder, {
            oid: oid,
            sid: sid,
            content: content,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function addOrderAllot(oid, pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addOrderAllot, {
            oid: oid,
            pid: pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getOrderStateList(oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderStateList, {
            oid: oid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getPayOrder(oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getPayOrder, {
            oid: oid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getOrderInvoice(oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderInvoice, {
            oid: oid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function confirmRefund(oid, type) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.confirmRefund, {
            oid: oid,
            type: type,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getOrdeExport(params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrdeExport, {
            excelType: params.excelType,
            providerId: params.providerId,
            key: params.key,
            status: params.status,
            parkId: params.parkId,
            type: params.type,
            dateType: params.dateType,
            orderPay: params.orderPay,
            isNeedlnvoice: params.isNeedlnvoice,
            refundStatus: params.refundStatus,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

export default {
    getOrders,
    getPayOrders,
    getOrderNum,
    getDetail,
    getOrderLogs,
    getOrderApply,
    getOrderState,
    getOrderContract,
    updateOrder,
    addOrderAllot,
    getOrderStateList,
    getPayOrder,
    getOrderInvoice,
    confirmRefund,
    getOrdeExport
}