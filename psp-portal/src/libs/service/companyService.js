import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getComInfoList': '/app/com/v1/getComInfoList', // 获取企业数据分析信息列表
    'getComInfo' : '/app/com/v1/getComInfo', // 获取企业数据分析信息详情
    'exportComs': '/app/com/v1/ExportComs', //文件-批量导出企业数据

    'getRecommendPolicy': '/app/com/v1/getRecommendPolicy', //获取推荐政策信息
    'editRecommendPolicy': '/app/com/v1/editRecommendPolicy', //编辑推荐政策关系
    'delRecommendPolicy': '/app/com/v1/delRecommendPolicy', //删除推荐政策信息

    'getComDictionary': '/app/com/v1/getComDictionary', //企业敏感词信息
    'editComDictionary': '/app/com/v1/editComDictionary', //编辑敏感词

    'getServiceEval': '/app/eval/v1/getServiceEval', //获取服务评价列表
    'getEvalDetail': '/app/eval/v1/getEvalDetail', //获取服务详情
    'evalTop':'/app/eval/v1/evalTop',  //置顶评价
    'evalDel': '/app/eval/v1/evalDel', //删除评价
    'evaluateExport': '/app/eval/v1/export ', //评价记录导出
    'officialEval': '/app/eval/v1/officialEval', //	官方回复
   
    'getQuestionList': '/app/question/v1/getQuestionList', //获取问题答疑列表
    'questionSave': '/app/question/v1/questionSave',  //	问题答疑保存
    'questionDel': '/app/question/v1/questionDel' //问题答疑删除
};

let res = {

};

function getComInfoList (params, page, pageSize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getComInfoList, {
            name: params.name,
            stage: params.stage,
            page: page,
            pageSize: pageSize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getComInfo (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getComInfo, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getRecommendPolicy (params, page, pageSize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getRecommendPolicy, {
            name: params.name,
            dicName: params.dicName,
            page: page,
            pageSize: pageSize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function exportComs (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.exportComs, params, {responseType: 'blob'}).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function editRecommendPolicy (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.editRecommendPolicy, {
            id: params.id,
            dicIds: JSON.stringify(params.dicIds),
            dics: JSON.stringify(params.dics),
            policys: JSON.stringify(params.policys)
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function delRecommendPolicy (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delRecommendPolicy, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getComDictionary (type, name, page, pagesize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getComDictionary, {
            type: type,
            name: name,
            page: page,
            pagesize: pagesize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function editComDictionary (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.editComDictionary, {
            id: params.id,
            name: params.name,
            type: params.type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getServiceEval (params, page, pageSize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getServiceEval, {
            name: params.name,
            provider: params.provider,
            demand: params.demand,
            replyState: params.replyState,
            addReply: params.addReply,
            startTime: params.startTime,
            endTime: params.endTime,
            page: page,
            pageSize: pageSize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getEvalDetail (eid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getEvalDetail, {
            eid: eid,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function evaluateExport (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.evaluateExport, params, {responseType: 'blob'}).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function evalTop (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.evalTop, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function evalDel (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.evalDel, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function officialEval (id, reply) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.officialEval, {
            id: id,
            reply: reply
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function getQuestionList (params, page, pageSize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getQuestionList, {
            name: params.name,
            startTime: params.startTime,
            endTime: params.endTime,
            page: page,
            pageSize: pageSize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function questionSave (params, page, pageSize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.questionSave, {
            id: params.id	,
            firstCid: params.firstCid,
            secCid: params.secCid,
            title: params.title,
            content: params.content,
            sort: params.sort,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function questionDel (id) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.questionDel, {
            id: id,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
export default {
    getComInfoList, 
    getComInfo,
    getRecommendPolicy,
    exportComs,
    editRecommendPolicy,
    delRecommendPolicy,
    getComDictionary,
    editComDictionary,
    getServiceEval,
    getEvalDetail,
    evaluateExport,
    evalTop,
    evalDel,
    officialEval,
    getQuestionList,
    questionSave,
    questionDel

}