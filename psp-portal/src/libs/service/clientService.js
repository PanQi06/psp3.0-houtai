import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getUsers': '/app/user/v1/getUsers', // 客户列表
    'getUserNum': '/app/user/v1/getUserNum', //获取客户总数
    'allot': '/app/user/v1/allot',//分配客户给销售
    'getDetail': '/app/user/v1/getDetail', //获取客户详情
    'getUserLogs': '/app/user/v1/getUserLogs',//获取客户操作日志
    'getUserNews': '/app/user/v1/getUserNews',//获取客户消息流
    'getUserOrdeExport': '/app/user/v1/getUserOrdeExport',// 客户需求导出

};

let res = {

};

function getUsers (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUsers, {
            page: params.currentPage,
            pagesize: params.pagesize,
            level: params.level,
            origin: params.origin,
            startCre: params.startCre,
            endCre: params.endCre,
            startAllot: params.startAllot,
            endAllot: params.endAllot,
            name: params.name,
            type: params.type,
            dataType: params.dataType,
            park: params.park,
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getUserNum (isAllot) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserNum, {
            isAllot: isAllot
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function allot (uid, sid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.allot, {
            uid: uid,
            sid: sid
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getDetail (oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getDetail, {
            oid: oid
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getUserLogs (oid, key) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserLogs, {
            oid: oid,
            key: key
        }, postCfg).then(function (res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}

function getUserNews (uid, oid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserNews, {
            uid: uid,
            oid: oid,
            page: params.currentPage,
            pagesize: params.pagesize,
            stype: params.stype,
            key: params.key
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getUserOrdeExport (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserOrdeExport, {
            level: params.level,
            origin: params.origin,
            startCre: params.startCre,
            endCre: params.endCre,
            startAllot: params.startAllot,
            endAllot: params.endAllot,
            name: params.name,
            type: params.type,
            dataType: params.dataType,
            park: params.park,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}


export default {
    getUsers: getUsers,
    getUserNum: getUserNum,
    allot: allot,
    getDetail: getDetail,
    getUserLogs: getUserLogs,
    getUserNews: getUserNews,
    getUserOrdeExport: getUserOrdeExport
}