import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}


let api = {
    'getUserInformation': '/app/user/v1/getUserInformation', //获取用户信息列表
    'userInformationExport': '/app/user/v1/userInformationExport'  //用户信息管理导出Excel
};

let res = {

};

function getUserInformation (params, page, pagesize) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getUserInformation, {
            name: params.name,
            type: params.type,
            page: page,
            pagesize: pagesize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function userInformationExport (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.userInformationExport, params, {responseType: 'blob'}).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}




export default {
    getUserInformation,
    userInformationExport

}