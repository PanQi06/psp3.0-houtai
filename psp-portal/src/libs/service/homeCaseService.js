import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getWebImgList': '/app/webImg/v1/getWebImgList', //获取首页服务案例信息列表
    'getWebImgDetail': '/app/webImg/v1/getWebImgDetail', //获取首页服务案例信息详情
    'updateWebImg': '/app/webImg/v1/updateWebImg', //设置是否展示、删除状态
    'addWebImg': '/app/webImg/v1/addWebImg', //新增、修改
   
};

let res = {

};

function getWebImgList (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getWebImgList, {
            type: params.type,
            page: params.currentPage,
            pagesize: params.pagesize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function updateWebImg (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateWebImg, {
            id: params.id,
            type: params.type,
            imgType: params.imgType,
            state: params.state
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function addWebImg (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addWebImg, {
            id: params.id,
            url: params.url,
            type: params.type
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}





export default {
    getWebImgList,
    updateWebImg,
    addWebImg
}