import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'getEnterpriseList': '/app/enterprise/v1/getEnterpriseList', //获取用户企业信息列表
    'getEnterpriseDetail': '/app/enterprise/v1/getEnterpriseDetail', //获取用户企业信息详情
   
};

let res = {

};

function getEnterpriseList (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getEnterpriseList, {
            key: params.key,
            page: params.page,
            pagesize: params.pagesize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getEnterpriseDetail (eid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getEnterpriseDetail, {
            eid: eid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}


export default {
    getEnterpriseList,
    getEnterpriseDetail
}