import Vue from 'vue'
import Router from 'vue-router'

import login from '@/view/login/login'
import LoginMainPanel from '@/view/login/view/LoginMainPanel'
import home from '@/view/home'
// 首页
import HomePanel from '@/view/HomePanel'
// 待办工作
import workReady from '@/view/workReady/workReady'
// 客户管理
import clientManage from '@/view/client/clientManage'
import clientDetail from '@/view/client/clientDetail'
// 工单管理
import workOrderManage from '@/view/workOrder/workOrderManage'
import workOrderDetail from '@/view/workOrder/workOrderDetail'
// 企业信息管理
import enterpriseManage from '@/view/enterprise/enterpriseManage'
import enterpriseDetail from '@/view/enterprise/enterpriseDetail'
// 服务分类管理
import serviceClassifyManage from '@/view/serviceClassify/serviceClassifyManage'
// 服务商管理
import serviceProvideManage from '@/view/serviceProvide/serviceProvideManage'
import serviceProvideDetail from '@/view/serviceProvide/serviceProvideDetail'
// 园区管理
import parkManage from '@/view/park/parkManage'
import parkDetail from '@/view/park/parkDetail'
// 客户经理管理
import sellerManage from '@/view/seller/sellerManage'
import sellerDetail from '@/view/seller/sellerDetail'
// 账号管理
import adminAccountManage from '@/view/adminAccount/adminAccountManage'
// 角色管理
import adminRoleManage from '@/view/adminAccount/adminRoleManage'
//个人资料管理
import userInfo from '@/view/userInfo/userInfo'
// 首页服务案例
import serviceCase from '@/view/homeServiceCase/serviceCasePage'
// 大屏预定
import reserveScreen from '@/view/reserveScreen/reserveScreen'

//大课堂订单分配
import bigClassOrderManage from '@/view/bigClassOrderManage/bigClassOrderManage'
//大课堂订单分配详情
import bigClassDetail from '@/view/bigClassOrderManage/view/bigClassDetail'
//政府调研订单分配
import governmentOrderManage from '@/view/governmentOrderManage/governmentOrderManage'
//政府调研订单详情
import govDetail from '@/view/governmentOrderManage/view/govDetail'
//企业数据分析
import companyData from '@/view/companyData/companyDataPage'
//企业数据分析详情
import companyDetail from '@/view/companyData/view/CompanyDetail'
//企业推荐政策管理
import companyRecommend from '@/view/companyRecommend/companyRecommend'
//敏感词管理
import wordManage from '@/view/wordManage/wordManage'
//评价管理
import evaluateManage from '@/view/evaluateManage/evaluateManage'
import evaluateDetail from '@/view/evaluateManage/view/evaluateDetail'
//用户信息
import userManage from '@/view/userManage/userManage'

//问题答疑
import problemManage from '@/view/problemManage/problemManage'
//新闻分类管理
import newsClassManage from '@/view/newsClassManage/newsClassManage'
//新闻内容管理
import newsConManage from '@/view/newsConManage/newsConManage'
//新闻内容添加
import newsConAdd from '@/view/newsConManage/view/newsConAdd'
//新闻详情管理
import newsConDetail from '@/view/newsConManage/view/newsConDetail'
//资质管理
import aptitudeManage from '@/view/aptitudeManage/aptitudeManage'
//资质详情
import aptitudeDetail from '@/view/aptitudeManage/view/aptitudeDetail'
//服务案例管理
import serviceCaseManage from '@/view/serviceCaseManage/serviceCaseManage'
//服务案例管理详情
import serviceCaseDetail from '@/view/serviceCaseManage/view/serviceCaseDetail'
//服务案例内容添加
import conAddDetail from '@/view/serviceCaseManage/view/conAddDetail'
//服务案例内容列表详情
import serviceCaseCon from '@/view/serviceCaseManage/view/serviceCaseCon'
//banner管理
import bannerManage from '@/view/bannerManage/bannerManage'
// 财务-七星
import financeServicePage from '@/view/finance/financeServicePage'
import detailServicePage from '@/view/finance/detailServicePage'
// 财务-大课堂
import financeClassPage from '@/view/finance/financeClassPage'
import detailClassPage from '@/view/finance/detailClassPage'
import {
  SSL_OP_NO_SESSION_RESUMPTION_ON_RENEGOTIATION
} from 'constants';

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'login',
      component: login,
      redirect: '/login',
      children: [{
        path: '/login',
        name: 'loginMainPanel',
        component: LoginMainPanel
      }]
    },
    {
      path: '/home',
      component: home,
      children: [{
          path: '/home',
          name: 'home',
          component: HomePanel
        },
        // 待办工作
        {
          path: '/workReady',
          name: 'workReady',
          component: workReady
        },
        // 客户管理
        {
          path: '/clientManage',
          name: 'clientManage',
          component: clientManage
        },
        {
          path: '/clientDetail',
          name: 'clientDetail',
          component: clientDetail
        },
        // 工单管理
        {
          path: '/workOrderManage',
          name: 'workOrderManage',
          component: workOrderManage
        },
        {
          path: '/workOrderDetail',
          name: 'workOrderDetail',
          component: workOrderDetail
        },
        // 企业信息管理
        {
          path: '/enterpriseManage',
          name: 'enterpriseManage',
          component: enterpriseManage
        },
        {
          path: '/enterpriseDetail',
          name: 'enterpriseDetail',
          component: enterpriseDetail
        },
        // 园区管理
        {
          path: '/parkManage',
          name: 'parkManage',
          component: parkManage
        },
        {
          path: '/parkDetail',
          name: 'parkDetail',
          component: parkDetail
        },
        // 客户经理管理
        {
          path: '/sellerManage',
          name: 'sellerManage',
          component: sellerManage
        },
        {
          path: '/sellerDetail',
          name: 'sellerDetail',
          component: sellerDetail
        },
        // 服务分类管理
        {
          path: '/serviceClassifyManage',
          name: 'serviceClassifyManage',
          component: serviceClassifyManage
        },
        // 服务商管理
        {
          path: '/serviceProvideManage',
          name: 'serviceProvideManage',
          component: serviceProvideManage
        },
        {
          path: '/serviceProvideDetail',
          name: 'serviceProvideDetail',
          component: serviceProvideDetail
        },
        // 首页服务案例
        {
          path: '/serviceCase',
          name: 'serviceCase',
          component: serviceCase
        },
        // 大屏预定
        {
            path: '/reserveScreen',
            name: 'reserveScreen',
            component: reserveScreen
          },
        // 账号管理
        {
            path: '/adminAccountManage',
            name: 'adminAccountManage',
            component: adminAccountManage
          },
        // 角色管理
        {
          path: '/adminRoleManage',
          name: 'adminRoleManage',
          component: adminRoleManage
        },
        // 个人资料管理
        {
          path: '/userInfo',
          name: 'userInfo',
          component: userInfo
        },
        // 大课堂订单分配
        {
          path: '/bigClassOrderManage',
          name: 'bigClassOrderManage',
          component: bigClassOrderManage
        },
        // 政府调研订单分配
        {
          path: '/governmentOrderManage',
          name: 'governmentOrderManage',
          component: governmentOrderManage
        },
        // 大课堂订单分配详情
        {
          path: '/bigClassOrderManage/:id',
          name: 'bigClassDetail',
          component: bigClassDetail
        },
        //政府调研订单详情
        {
          path: '/governmentOrderManage/:id',
          name: 'govDetail',
          component: govDetail
        },
        //企业数据分析
        {
          path: '/companyData',
          name: 'companyData',
          component: companyData
        },
        {
          path: '/companyData/:id',
          name: 'companyDetail',
          component: companyDetail
        },
        //企业推荐政策管理
        {
          path: '/companyRecommend',
          name: 'companyRecommend',
          component: companyRecommend
        },
        //敏感词管理
        {
          path: '/wordManage',
          name: 'wordManage',
          component: wordManage
        },
        //评价管理
        {
          path: '/evaluateManage',
          name: 'evaluateManage',
          component: evaluateManage
        },
        {
          path: '/evaluateManage/:id',
          name: 'evaluateDetail',
          component: evaluateDetail
        },
        //用户信息
        {
          path: '/userManage',
          name: 'userManage',
          component: userManage
        },
        //问题答疑
        {
          path: '/problemManage',
          name: 'problemManage',
          component: problemManage
        },
        //新闻分类管理
        {
          path: '/newsClassManage',
          name: 'newsClassManage',
          component: newsClassManage
        },
        //新闻内容管理
        {
          path: '/newsConManage',
          name: 'newsConManage',
          component: newsConManage
        },
        //新闻详情
        {
          path: '/newsConManage/:id',
          name: 'newsConDetail',
          component: newsConDetail
        },
        // 新闻内容添加
        {
          path: '/newsConAdd',
          name: 'newsConAdd',
          component: newsConAdd
        },
        //资质管理
        {
          path: '/aptitudeManage',
          name: 'aptitudeManage',
          component: aptitudeManage
        },
        //资质详情
        {
          path: '/aptitudeManage/:id',
          name: 'aptitudeDetail',
          component: aptitudeDetail
        },
        //服务案例管理
        {
          path: '/serviceCaseManage',
          name: 'serviceCaseManage',
          component: serviceCaseManage
        },
        //服务案例管理详情
        {
          path: '/serviceCaseDetail/:id',
          name: 'serviceCaseDetail',
          component: serviceCaseDetail
        },
        //服务案例内容添加
        {
          path: '/conAddDetail',
          name: 'conAddDetail',
          component: conAddDetail
        },
        //服务案例内容列表详情
        {
          path: '/serviceCaseCon/:id',
          name: 'serviceCaseCon',
          component: serviceCaseCon
        },
        //banner管理
        {
          path: '/bannerManage',
          name: 'bannerManage',
          component: bannerManage
        },
        // 财务-七星
        {
          path: '/financeService',
          name: 'financeService',
          component: financeServicePage
        },
        {
          path: '/detailService',
          name: 'detailService',
          component: detailServicePage
        },
        // 财务-大课堂
        {
          path: '/financeClass',
          name: 'financeClass',
          component: financeClassPage
        },
        {
          path: '/detailClass',
          name: 'detailClass',
          component: detailClassPage
        },
      ]
    },
    {
      path: '*',
      component: LoginMainPanel
    }
  ]
})
