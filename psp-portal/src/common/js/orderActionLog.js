let formatActionType = function (type) {
    if (type == 1) {
      return '订单待分配';
    } else if (type == 2) {
      return '订单已分配';
    } else if (type == 3) {
      return '订单成立';
    } else if (type == 4) {
      return '建立合同';
    } else if (type == 5) {
      return '确认支付';
    } else if (type == 6) {
      return '确认收款'
    } else if (type == 7) {
      return '申请完成'
    } else if (type == 8) {
      return '订单完成'
    } else if (type == 9) {
      return '订单终止'
    } else if (type == 10) {
      return '推送支付链接'
    } else if (type == 11) {
      return '用户申请退款'
    } else if (type == 12) {
      return '用户申请终止工单'
    } else if (type == 13) {
      return '服务商申请终止工单'
    } else if (type == 14) {
      return '订单推送财务'
    } else if (type == 15) {
      return '填写快递信息'
    } else if (type == 16) {
      return '销售同意用户申请'
    } else if (type == 17) {
      return '销售同意服务商申请'
    } else if (type == 18) {
      return '销售拒绝用户申请'
    } else if (type == 19) {
        return '拒绝完成';
    } else if (type == 20) {
      return '开具发票'
    } else if (type == 21) {
        return '服务商拒绝'
    } else if (type == 22) {
        return '财务同意退款'
    } else if (type == 23) {
        return '财务拒绝退款'
    }
  }
  
  let formatAction = function (data) {
    let type = data.type;
    if (type == 1) {
        return '工单等待分配';
    } else if (type == 2) {
      let seller = JSON.parse(data.sellerJson);
      if (data.adminJson != null) {
        let admin = JSON.parse(data.adminJson);
        return '管理员【' + admin.name + '】将工单重新分配给客户经理【' + seller.name + '】';
      } else {
        let provider = JSON.parse(data.providerJson);
        return '客户经理【' + seller.name + '】创建工单并分配给服务商【' + provider.name + '】';
      }
    } else if (type == 3) {
      let provider = JSON.parse(data.providerJson);
      return '服务商【' + provider.name + '】接受工单';
    } else if (type == 4) {
      let seller = JSON.parse(data.sellerJson);
      let content = JSON.parse(data.content);
      if (content.type == 1) {
        return '客户经理【' + seller.name + '】上传用户合同，合同编号：' + content.orderNo;
      } else {
        return '客户经理【' + seller.name + '】上传服务商合同，合同编号：' + content.orderNo;
      }
    } else if (type == 5) {
      if (data.orderType == 1) {
        return '用户确认支付';
      } else {
        let seller = JSON.parse(data.sellerJson);
        return '客户经理【' + seller.name + '】确认用户支付';
      }
    } else if (type == 6) {
      let admin = JSON.parse(data.adminJson);
      return '财务【' + admin.name + '】确认收款';
    } else if (type == 7) {
      let provider = JSON.parse(data.providerJson);
      return '服务商【' + provider.name + '】申请完成工单';
    } else if (type == 8) {
      return '工单已完成';
    } else if (type == 9) {
      return '工单已终止'
    } else if (type == 10) {
      let seller = JSON.parse(data.sellerJson);
      return '客户经理【' + seller.name + '】给用户推送支付链接';
    } else if (type == 11) {
      return '用户申请退款';
    } else if (type == 12) {
      return '用户申请终止合同';
    } else if (type == 13) {
      return '服务商申请终止工单';
    } else if (type == 14) {
      let seller = JSON.parse(data.sellerJson);
      return '客户经理【' + seller.name + '】给财务推送订单';
    } else if (type == 15) {
      let seller = JSON.parse(data.sellerJson);
      return '客户经理【' + seller.name + '】填写快递信息';
    } else if (type == 16) {
      let seller = JSON.parse(data.sellerJson);
      return '客户经理【' + seller.name + '】同意用户申请';
    } else if (type == 17) {
      let seller = JSON.parse(data.sellerJson);
      let provider = JSON.parse(data.providerJson);
      return '客户经理【' + seller.name + '】同意服务商【' + provider.name + '】申请';
    } else if (type == 18) {
      let seller = JSON.parse(data.sellerJson);
      return '客户经理【' + seller.name + '】拒绝用户申请';
    } else if (type == 19) {
      let seller = JSON.parse(data.sellerJson);
      let provider = JSON.parse(data.providerJson);
      return '客户经理【' + seller.name + '】拒绝服务商【' + provider.name + '】申请';
    } else if (type == 20) {
      let admin = JSON.parse(data.adminJson);
      return '财务【' + admin.name + '】开具发票';
    } else if (type == 21) {
        return '服务商拒绝接受工单'
    } else if (type == 22) {
        let admin = JSON.parse(data.adminJson);
        return '财务【' + admin.name + '】同意退款';
    } else if (type == 23) {
        let admin = JSON.parse(data.adminJson);
        return '财务【' + admin.name + '】拒绝退款';
    }
  }
  
  export {
    formatActionType,
    formatAction
  }
  