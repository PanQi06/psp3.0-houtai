import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    //工单相关接口
    'getOrders': '/app/order/v1/getOrders', //获取工单信息列表
    'getOrderNum': '/app/order/v1/getOrderNum', //获取工单数量
    'getDetail': '/app/order/v1/getDetail', //获取工单详情
    'getOrderLogs': '/app/order/v1/getOrderLogs', //获取工单操作日志
    'accept': '/app/order/v1/accept',//接受工单
    'refuse': '/app/order/v1/refuse',//拒绝工单
    'submitFinish': '/app/order/v1/submitFinish',//申请完成工单
    'submitClose': '/app/order/v1/submitClose',//申请终止工单
};

let res = {

};
function getOrders (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrders, {
            page: params.currentPage,
            pagesize: params.pagesize,
            stage: params.stage,
            stype: params.stype,
            key: params.key,
            filteType: params.filteType
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getOrderNum(filteType) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderNum, {
            stage: 0,
            filteType: filteType
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getDetail(oid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getDetail, {
            oid: oid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function getOrderLogs(oid, key) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getOrderLogs, {
            oid: oid,
            key: key
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function accept(oid, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.accept, {
            oid: oid,
            content: content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function refuse(oid, content) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.refuse, {
            oid: oid,
            content: content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function submitFinish(oid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.submitFinish, {
            oid: oid,
            type: params.type,
            content: params.content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
function submitClose(oid, params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.submitClose, {
            oid: oid,
            type: params.type,
            content: params.content
        }, postCfg).then(function(res) {
            resolve(res);
        }, function (error) {
            reject(error);
        });
    });
    return p;
}
export default {
    getOrders, getOrderNum, getDetail, getOrderLogs, accept, refuse, submitFinish, submitClose
}