import util from '@/libs/common/util.js';
import $ from 'jquery'

let postCfg = {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    transformRequest: function (data) {
        return $.param(data)
    }
}

let api = {
    'login': '/app/account/v1/login2Pwd',
    'resetPwd': '/app/account/v1/update2Pwd',//更改密码
    'updateName': '/app/account/v1/update2Name',//更改用户名
    'getAccountList': '/app/account/v1/accountList',//获取服务商列表
    'addAccount': '/app/account/v1/accountAdd',//创建服务商账号
    'resetAccountPwd': '/app/account/v1/accountPwdReset',//重置服务商账号密码
    'delAccount': '/app/account/v1/accountDel',//删除服务商账号
    'logout': '/app/account/v1/logout',//退出登录
};

let res = {

};
function login (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.login, {
            phoneNum: params.phoneNum,
            pwd: params.password,
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function resetPwd (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.resetPwd, {
            oldPwd: params.oldPwd,
            pwd: params.password,
            confirmPwd: params.confirmPwd
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function updateName (name) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.updateName, {
            name: name
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function getAccountList (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.getAccountList, {
            page: params.currentPage,
            pagesize: params.pagesize
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function addAccount (params) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.addAccount, {
            name: params.name,
            phone: params.phone,
            password: params.pwd
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function resetAccountPwd (pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.resetAccountPwd, {
            pid: pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
function delAccount (pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.delAccount, {
            pid: pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}

function logout (pid) {
    let p = new Promise(function (resolve, reject) {
        util.ajax.post(api.logout, {
            pid: pid
        }, postCfg).then(function(res) {
            resolve(res);
        }, function(error) {
            reject(error);
        });
    });
    return p;
}
export default {
    login,resetPwd,updateName,getAccountList,addAccount,resetAccountPwd,delAccount,logout
}