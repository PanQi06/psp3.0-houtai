// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import appUtil from '@/libs/common/util'
require('./common/css/reset.css')

import store from '@/store/store.js'

import iView from 'iview';
import '@/my-theme/my-theme.less';
Vue.use(iView);
//全局ajax请求拦截器
appUtil.ajax.interceptors.request.use(

    config => {
      let token = store.getters.token;
      if (token) { // 判断是否存在token，如果存在的话，则每个http header都加上token
        config.headers['token'] = token;
      }
      // config.headers['content-type'] = 'application/x-www-form-urlencoded';
      return config;
    },
    err => {
      return Promise.reject(err);
    });
  //响应拦截器
  appUtil.ajax.interceptors.response.use(
    response => {
      // if (response.data.token != 'undefined' && response.data.token != null) {
      //     localStorage.setItem('userTOKEN', response.token);
      // }
      if (response.data.rescode == 102) {
        router.push({
          name: 'login'
        })
      }
  
      return response;
    },
    error => {
      return Promise.reject(error);
    });
Vue.prototype.axios = axios
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
